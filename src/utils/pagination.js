export function getUrlParams(list) {

  let query = list.searchParams.reduce((prev, cur) => {
    if (!!cur.field && cur.query)
      return (prev += `&search=${cur.field}:${cur.query}`);

    return prev;
  }, '').substring(1);

  let sort
  if (!list.column)  {
    sort = 'sort=id,asc';
  } else {
    sort = `sort=${list.column},${list.direction}`
  }

  return `${sort}&${query}`;
}
