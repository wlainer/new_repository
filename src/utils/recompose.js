import { withState, withHandlers } from 'recompose'
import { search } from '../actions/api';
import {get} from 'lodash'


export const withStateSearch = withState('results', 'setResult', [])


export const withHandlerSearch = withHandlers(
  {
    handleSearch: ({ setResult }) => filter => async (e, { value }) => {
      if (value.length < 3) return
      const results = await search(`${filter.url}${value}`);
      const toReturn = results.content.map(e => ({
        ...e,
        title: get(e, filter.attribute, "")
      }));
      setResult(toReturn)
    }
  }
)
