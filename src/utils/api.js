import FetchApi from './fetchApi';
import { isEmpty } from 'lodash';

// @flow
export default class Api extends FetchApi {
  static async getAll(endpoint) {
    try {
      const response = await super.get(endpoint);
      if (response.status === 200) return response.data;
    } catch (e) {
      throw new Error(e.message);
    }
  }

  static async get(endpoint, id) {
    try {
      const response = await super.get(`${endpoint}\\${id}`);
      if (response.status === 200) return response.data;
    } catch (e) {
      throw new Error(e.message);
    }
  }

  static async save(endpoint, data) {
    try {
      const response = await super.post(endpoint, data);
      if (response.status === 200) return response.data;
    } catch (e) {
      throw new Error(e.message);
    }
  }

  static async delete(endpoint, id) {
    try {
      const response = await super.delete(`${endpoint}\\${id}`);
      if (response.status === 200) return response.data;
    } catch (e) {
      throw new Error(e.message);
    }
  }

  static async search(url) {
    try {
      const response = await super.get(url);
      if (response.status === 200) return response.data;
    } catch (e) {
      throw new Error(e.message);
    }
  }
}
