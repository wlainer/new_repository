export function insertItem(array, index, item) {
  let newArray = array.slice();
  newArray.splice(index, 0, item);
  return newArray;
}
export function removeItem(array, index) {
  let newArray = array.slice();
  newArray.splice(index, 1);
  return newArray;
}
export function replaceItem(array, idx, item) {
  return Object.assign([], array, { [idx]: item });
}
