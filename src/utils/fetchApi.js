import axios from 'axios'
import {isEmpty} from 'lodash'

// @flow
export default class FetchApi {

  static baseEndpoint: string = 'http://localhost:8080'

  static config (headers: Object = {}, anyParam: Object = {}): Object {
    return {
      headers: this.headers(headers),
      ...anyParam
    }
  }

  static headers(headerParams: Object = {}): Object {
    return {
      "content-type": "application/json",
      ...headerParams
    }
  }

  static setConfig(token: string = ''): Object {
    let config = this.config()

    if (!isEmpty(token)) {
      config = Object.assign({}, this.config(this.headers({"Authorization": `JWT ${token}`})))
    }

    return config
  }

  static async post(url: string= '', data: Object = {}, token: string = '') {
    const config = this.setConfig(token)
    return await axios.post(`${this.baseEndpoint}/${url}`, data, config)
  }

  static async get(url: string = '', token: string = '') {
    const config = this.setConfig(token)
    return await axios.get(`${this.baseEndpoint}/${url}`, config)
  }

  static async delete(url: string = '', token: string = '') {
    const config = this.setConfig(token)
    return await axios.delete(`${this.baseEndpoint}/${url}`, config)
  }

}
