import { PAGINATION_RESET, PAGINATION_UPDATE } from '../constants/actionTypes';

import objectAssign from 'object-assign';
import initialState from './initialState';

export default function searchBarReducer(state = initialState.pagination,action,) {
  let newState;

  switch (action.type) {
    case PAGINATION_UPDATE:
      const {type, ...rest} = action
      newState = Object.assign({}, state, rest);

      return newState;

    case PAGINATION_RESET:
      const newPagination = Object.assign({}, initialState.pagination);
      newState = objectAssign({}, state, newPagination);
      return newState;

    default:
      return state;
  }
}
