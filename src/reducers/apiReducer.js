import {
  API_CLEAR,
  API_CLEAR_ITEM,
  API_FETCH,
  API_FETCH_SUCCESS,
  API_FETCH_ERROR,
} from '../constants/actionTypes';

import objectAssign from 'object-assign';
import initialState from './initialState';

export default function searchBarReducer(state = initialState.api, action) {
  let newState;

  switch (action.type) {
    case API_FETCH:
      newState = Object.assign({}, state, { isLoading: action.isLoading });
      return newState;

    case API_FETCH_SUCCESS:
      const { type, ...rest } = action;
      newState = Object.assign({}, state, rest);
      return newState;

    case API_FETCH_ERROR:
      return newState;

    case API_CLEAR:
      newState = Object.assign({}, state, { item: {} });
      return newState;

    case API_CLEAR_ITEM:
      newState = Object.assign({}, state, { item: {} });
      return newState;

    default:
      return state;
  }
}
