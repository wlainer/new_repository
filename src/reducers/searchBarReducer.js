import { SEARCHBAR_RESET, SEARCHBAR_UPDATE, SEARCHBAR_UPDATE_SORT } from '../constants/actionTypes';
import initialState from './initialState';
import { insertItem, removeItem, replaceItem } from '../utils/array';

export default function searchBarReducer(state = initialState.searchBar, action) {
  let newState;

  switch (action.type) {
    case SEARCHBAR_UPDATE:
      const newArray = replaceItem(state.searchParams, action.index, action.item);
      newState = Object.assign({}, state, { searchParams: [...newArray] });
      return newState;

    case SEARCHBAR_UPDATE_SORT:
      newState = Object.assign({}, state, { ...action.sort }, { searchParams: [...initialState.searchBar.searchParams] })
      return newState

    case SEARCHBAR_RESET:
      newState = Object.assign({}, initialState.searchBar);
      return newState;

    default:
      return state;
  }
}
