export default {
  searchBar: {
    column: '',
    direction: '',
    searchParams: [
      {
        field: '',
        query: '',
      },
    ],
  },
  pagination: {
    size: 10,
    currentPage: 0,
    totalPages: 0,
  },
  api: {
    isLoading: false,
    items: [],
    item: {},
  },
};
