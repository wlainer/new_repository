import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import pagination from './paginationReducer';
import searchBar from './searchBarReducer';
import api from './apiReducer';

const rootReducer = combineReducers({
  api,
  searchBar,
  pagination,
  routing: routerReducer,
});

export default rootReducer;
