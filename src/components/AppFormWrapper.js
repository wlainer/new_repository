import React from 'react';
import PropTypes from 'prop-types';

const AppWrapper = BaseComponent => {
  return class extends React.Component {
    constructor(props) {
      super(props);

      this.handleActions = this.handleActions.bind(this);
    }

    componentDidMount() {
      const { match } = this.props;
      const id = match.params.id;

      if (!id) return;

      this.props.get(id);
    }

    componentWillUnmount() {
      this.props.clearItem();
    }

    handleActions(action) {
      return values => {
        const { history, location } = this.props;
        const myRegexp = /^(\/.*?)\//;
        const match = myRegexp.exec(location.pathname);
        const url = match[1];

        const path = location.pathname;
        switch (action) {
          case 'save':
            // handle formData
            if (!!values.formType) {
              const { file, formType, ...object } = values

              const headers = { 'Content-Type': 'multipart/form-data' };

              const form = new FormData();
              form.set('object', JSON.stringify(object));

              if (!!file) form.set('file', file);

              values = {
                form,
                headers
              }
            }

            this.props.save(values);
            history.push(url, { reset: false });
            break;
          case 'cancel':
            history.push(url, { reset: false });
            break;
          default:
        }
      };
    }

    render() {
      const { item, isLoading } = this.props;

      return (
        <BaseComponent
          isLoading={isLoading}
          item={item}
          handleSubmit={this.handleActions('save')}
          handleSubmitFormData={this.handleSubmitFormData}
          handleCancel={this.handleActions('cancel')}
        />
      );
    }
  };
};

export default AppWrapper;
