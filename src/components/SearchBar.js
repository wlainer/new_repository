import React from 'react';
import PropTypes from 'prop-types';
import {
  Icon,
  Grid,
  Button,
  Segment,
  Container,
  Input,
  Header,
  Dropdown,
} from 'semantic-ui-react';


const SearchBar = ({ items, handleChange, fields, onReset, onSearch }) => {
  const f = fields.map(field => ({
    key: typeof field === 'string' ? field.toLowerCase() : field.value,
    value: typeof field === 'string' ? field.toLowerCase() : field.value,
    text: typeof field === 'string' ? field : field.text,
  }));

  return (
    <Segment.Group>
      <Segment color="blue" inverted>
        <Header as="h5" className="search-bar-title">
          Pesquisar
        </Header>
      </Segment>
      <Segment>
        <Grid>
          <Grid.Row className="search-bar-header">
            <Grid.Column width={4}>Campo</Grid.Column>
            <Grid.Column width={12}>Critério</Grid.Column>

          </Grid.Row>

          {items.map((searchParam, idx) => (
            <Grid.Row key={idx}>
              <Grid.Column width={4}>
                <Dropdown
                  onChange={(event, data) => handleChange('field', idx)(data)}
                  value={items[idx]['field']}
                  placeholder="Campo"
                  search
                  selection
                  fluid
                  options={f}
                />
              </Grid.Column>
              <Grid.Column width={12}>
                <Input
                  onChange={(event, data) => handleChange('query', idx)(data)}
                  value={items[idx]['query']}
                  placeholder="Pesquisar..."
                  fluid
                />
              </Grid.Column>

            </Grid.Row>
          ))}

          <Grid.Row columns={1}>
            <Container style={{ paddingRight: '16px' }}>
              <Button
                color="green"
                size="small"
                floated="right"
                onClick={() => onSearch()}
              >
                <Icon name="search" />
                Pesquisar
              </Button>
              <Button
                color="yellow"
                size="small"
                floated="right"
                onClick={() => onReset()}
              >
                <Icon name="erase" />
                Limpar Campos
              </Button>
            </Container>
          </Grid.Row>
        </Grid>
      </Segment>
    </Segment.Group>
  );
};

SearchBar.propTypes = {
  items: PropTypes.array.isRequired,
  handleChange: PropTypes.func.isRequired,
  fields: PropTypes.oneOfType(
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.arrayOf(PropTypes.string),
  ).isRequired,
  onReset: PropTypes.func.isRequired,
  onSearch: PropTypes.func.isRequired,
};

export default SearchBar;
