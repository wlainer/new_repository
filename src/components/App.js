/* eslint-disable import/no-named-as-default */
import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import { Container } from 'semantic-ui-react';

import HomePage from './HomePage';

import AutorizacoesContainer from './containers/AutorizacoesContainer';
import AutorizacaoFormContainer from './containers/AutorizacaoFormContainer';

import PacientesContainer from './containers/PacientesContainer';
import PacienteFormContainer from './containers/PacienteFormContainer';

import PrestadoresContainer from './containers/PrestadoresContainer';
import PrestadorFormContainer from './containers/PrestadorFormContainer';

import ServicosContainer from './containers/ServicosContainer';
import ServicoFormContainer from './containers/ServicoFormContainer';

import NotFoundPage from './NotFoundPage';
import Navbar from './Navbar';

// This is a class-based component because the current
// version of hot reloading won't hot reload a stateless
// component at the top-level.

class App extends React.Component {
  constructor(props) {
    super(props);

    this.config = {
      pacientes: {
        headers: ['Matrícula', 'Paciente', 'Titular'],
        fields: ['matricula', 'paciente', 'titular'],
        searchFields: [
          {
            value: 'matricula',
            text: 'Matricula',
          },
          'Paciente',
          'Nome',
        ],
      },
      prestadores: {
        headers: ['Nome'],
        fields: ['nome'],
        searchFields: ['Nome'],
      },
      servicos: {
        headers: ['Nome', "Descrição"],
        fields: ['nome', 'descricao'],
        searchFields: ['Nome', "Descricao"],
      },
      autorizacoes: {
        headers: ['Núm', 'Data Início', 'Data Fim', 'Paciente'],
        fields: [
          'numeroAutorizacao',
          'dataInicio',
          'dataFim',
          'paciente.paciente',
        ],
        searchFields: [
          {
            value: 'numAutorizacao',
            text: 'Núm. Autorização',
          },
          {
            value: 'dataInicio',
            text: 'Data Início',
          },
          {
            value: 'dataFim',
            text: 'Data Fim',
          },
        ],
      },
    };
  }

  render() {
    return (
      <div>
        <Navbar />
        <div className="container">
          <Container>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route
                exact
                path="/pacientes"
                render={props => (
                  <PacientesContainer {...this.config.pacientes} />
                )}
              />
              <Route exact path="/pacientes/new" component={PacienteFormContainer} />
              <Route exact path="/pacientes/:id/edit" component={PacienteFormContainer} />

              <Route
                exact
                path="/prestadores"
                render={props => (
                  <PrestadoresContainer {...this.config.prestadores} />
                )}
              />
              <Route exact path="/prestadores/new" component={PrestadorFormContainer} />
              <Route exact path="/prestadores/:id/edit" component={PrestadorFormContainer} />

              <Route
                exact
                path="/autorizacoes"
                render={props => (
                  <AutorizacoesContainer {...this.config.autorizacoes} />
                )}
              />
              <Route exact path="/autorizacoes/new" component={AutorizacaoFormContainer} />
              <Route exact path="/autorizacoes/:id/edit" component={AutorizacaoFormContainer} />

              <Route
                exact
                path="/servicos"
                render={props => (
                  <ServicosContainer {...this.config.servicos} />
                )}
              />
              <Route exact path="/servicos/new" component={ServicoFormContainer} />
              <Route exact path="/servicos/:id/edit" component={ServicoFormContainer} />
              <Route component={NotFoundPage} />
            </Switch>
          </Container>
        </div>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.element,
};

export default App;
