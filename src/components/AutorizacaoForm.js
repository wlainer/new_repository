import React from 'react';
import {
  Button,
  Form,
  Grid,
  Divider,
  Icon,
  Input,
  TextArea,
  Search,
} from 'semantic-ui-react';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import { compose } from 'recompose';
import { withStateSearch, withHandlerSearch } from '../utils/recompose';

import moment from 'moment';

import FormService from '../utils/form';
import FetchApi from '../utils/fetchApi';

const AutorizacaoForm = ({
  item,
  handleSubmit,
  handleCancel,
  isLoading,
  handleSearch,
  results,
}) => {
  const filters = {
    paciente: {
      url: 'pacientes?search=paciente:',
      attribute: 'paciente',
    },
    prestador: {
      url: 'prestadores?search=nome:',
      attribute: 'nome',
    },
  };

  const autorizacao = Object.assign({}, item, { formType: 'formData' });

  return (
    <Grid celled>
      <Grid.Column>
        <FormService initialValues={autorizacao} onSubmit={handleSubmit}>
          {({ getValue, getError, setValue, submit, isValid, isTouched }) => (
            <Form loading={isLoading}>
              <Form.Group>
                <Form.Field>
                  <label>Data Início</label>
                  <DatePicker
                    onChange={e => setValue('dataInicio', e.format())}
                    selected={
                      !getValue('dataInicio')
                        ? ''
                        : moment(getValue('dataInicio'))
                    }
                  />
                </Form.Field>

                <Form.Field>
                  <label>Data Fim</label>
                  <DatePicker
                    onChange={e => setValue('dataFim', e)}
                    selected={
                      !getValue('dataFim') ? '' : moment(getValue('dataFim'))
                    }
                  />
                </Form.Field>
              </Form.Group>

              <Form.Field>
                <label>Número Autorização</label>
                <Input
                  placeholder="Número autorização"
                  onChange={e => setValue('numeroAutorizacao', e.target.value)}
                  value={getValue('numeroAutorizacao')}
                />
              </Form.Field>

              <Form.Field>
                <label>Prestador</label>
                <Search
                  loading={isLoading}
                  minCharacters={3}
                  onResultSelect={(e, { result }) => {
                    delete result.title;
                    setValue('prestador', result);
                  }}
                  onSearchChange={(e, { value }) => {
                    setValue('prestador.nome', value);
                    handleSearch(filters.prestador)(e, { value });
                  }}
                  results={results}
                  value={getValue('prestador.nome')}
                />
              </Form.Field>

              <Form.Field>
                <label>Paciente</label>
                <Search
                  loading={isLoading}
                  minCharacters={3}
                  onResultSelect={(e, { result }) => {
                    delete result.title;
                    setValue('paciente', result);
                  }}
                  onSearchChange={(e, { value }) => {
                    setValue('paciente.paciente', value);
                    handleSearch(filters.paciente)(e, { value });
                  }}
                  results={results}
                  value={getValue('paciente.paciente')}
                />
              </Form.Field>

              <Form.Field>
                <label>Observação</label>
                <TextArea
                  placeholder="Observação"
                  onChange={e => setValue('observacao', e.target.value)}
                  value={getValue('observacao')}
                />
              </Form.Field>

              <Form.Field>
                <label>Arquivo</label>
                <Grid columns={2} celled divided>
                  <Grid.Row>
                    <Grid.Column>
                      <a
                        href={`${FetchApi.baseEndpoint}/autorizacoes/${getValue(
                          'id',
                        )}/file`}
                        target="_blank"
                      >
                        <Icon name="file" /> {getValue('filename')}
                      </a>
                    </Grid.Column>
                    {/* <Grid.Column>
                      <a href={`${FetchApi.baseEndpoint}/autorizacoes/${getValue('id',)}/file`}target="_blank">
                        <Icon name="delete" /> 
                      </a>
                    </Grid.Column> */}
                  </Grid.Row>
                </Grid>
              </Form.Field>

              <Form.Field>
                <label>Anexo</label>
                <input
                  type="file"
                  onChange={e => setValue('file', e.target.files[0])}
                />
              </Form.Field>

              <Divider />
              <Button
                size="small"
                onClick={() => submit()}
                floated="right"
                disabled={!isValid()}
                primary
              >
                <Icon name="save" />
                Salvar
              </Button>
              <Button
                size="small"
                onClick={() => handleCancel()}
                floated="right"
                negative
              >
                <Icon name="cancel" />
                Cancelar
              </Button>
            </Form>
          )}
        </FormService>
      </Grid.Column>
    </Grid>
  );
};

export default compose(withStateSearch, withHandlerSearch)(AutorizacaoForm);
