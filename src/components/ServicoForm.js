import React from 'react';
import { Button, Form, Grid, Divider, Icon, Input } from 'semantic-ui-react';

import FormService from '../utils/form';

const ServicoForm = ({ item, handleSubmit, handleCancel, isLoading }) => (
  <Grid celled>
    <Grid.Column>
      <FormService
        initialValues={item}
        onSubmit={handleSubmit}
        validations={{
          nome: [value => !value && 'Obrigatório'],
        }}
      >
        {({ getValue, getError, setValue, submit, isValid, isTouched }) => (
          <Form loading={isLoading}>
            <Form.Field error={isTouched('nome') && getError('nome')} required>
              <label>Nome</label>
              <Input
                placeholder="Nome"
                onChange={e => setValue('nome', e.target.value)}
                value={getValue('nome')}
              />
            </Form.Field>
            <Form.Field>
              <label>Descrição</label>
              <Input
                placeholder="Descrição"
                onChange={e => setValue('descricao', e.target.value)}
                value={getValue('descricao')}
              />
            </Form.Field>
            <Divider />
            <Button
              onClick={() => submit()}
              floated="right"
              disabled={!isValid()}
              primary
            >
              <Icon name="save" />
              Salvar
            </Button>
            <Button onClick={() => handleCancel()} floated="right" negative>
              <Icon name="cancel" />
              Cancelar
            </Button>
          </Form>
        )}
      </FormService>
    </Grid.Column>
  </Grid>
);

export default ServicoForm;
