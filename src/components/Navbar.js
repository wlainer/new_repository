import React from 'react';
import { NavLink } from 'react-router-dom';

const Navbar = () => {

  return (
    <div className="navbar">
      <nav>
        <ul>
          <li>
            <NavLink exact to="/" activeClassName="active">
              Home
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/autorizacoes" activeClassName="active">
              Autorizações
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/pacientes" activeClassName="active">
              Pacientes
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/prestadores" activeClassName="active">
              Prestadores
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/servicos" activeClassName="active">
              Serviços
            </NavLink>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;
