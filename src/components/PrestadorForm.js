import React from 'react';
import {
  Button,
  Form,
  Grid,
  Divider,
  Icon,
  Input,
  Search,
  Card,
} from 'semantic-ui-react';
import { cloneDeep, get } from 'lodash'

import { compose, withState } from 'recompose';
import { withStateSearch, withHandlerSearch } from '../utils/recompose';

import FormService from '../utils/form';

const PrestadorForm = ({
  item,
  handleSubmit,
  handleCancel,
  isLoading,
  handleSearch,
  results,
  servicos,
  setServico
}) => {
  const filters = {
    servico: {
      url: 'servicos?search=nome:',
      attribute: 'nome',
    },
  };

  const teste = cloneDeep(item)

  if (servicos.length === 0 && !!teste.prestadoresServico) {
    setServico(teste.prestadoresServico)
  }

  const addServico = () => {
    if (!servico.id)
      return
    setServico(props => (
      [...props, { servico }]
    ))
  }

  let servico = {}

  return (
    <Grid celled>
      <Grid.Column>
        <FormService
          initialValues={teste}
          onSubmit={handleSubmit}
          validations={{
            nome: [value => !value && 'Obrigatório'],
          }}
        >
          {({
            getValue,
            getError,
            setValue,
            submit,
            isValid,
            isTouched,
            values
          }) => (
              <Form loading={isLoading}>
                <Form.Field
                  error={isTouched('nome') && getError('nome')}
                  required
                >
                  <label>Nome</label>
                  <Input
                    placeholder="Nome"
                    onChange={e => setValue('nome', e.target.value)}
                    value={getValue('nome')}
                  />
                </Form.Field>

                <Form.Group>
                  <Form.Field width={5}>
                    <label>Serviços</label>
                    <Search
                      loading={isLoading}
                      minCharacters={3}
                      onResultSelect={(e, { result }) => {
                        delete result.title;
                        servico = result
                        // setValue('prestador', result);
                      }}
                      onSearchChange={(e, { value }) => {
                        // setValue('prestador.nome', value);
                        handleSearch(filters.servico)(e, { value });
                      }}
                      results={results}
                    // value={getValue('prestador.nome')}
                    />
                  </Form.Field>
                  <Form.Button onClick={() => addServico()} style={{ marginTop: 18 }}>
                    <Icon name="add" /> Adicionar
                </Form.Button>
                </Form.Group>

                <Card.Group>
                  {!!servicos &&
                    servicos.map((e, idx) => (
                      <Card key={idx}>
                        <Card.Content>
                          <Card.Header>
                            {get(e, 'servico.nome', '')}
                          </Card.Header>
                        </Card.Content>
                        <Card.Content extra>
                          <Input fluid
                            value={get(e, 'valor', '')}
                            onChange={(ev) => {
                              ev.persist()
                              setServico(props => {
                                props[idx].valor = ev.target.value
                                return [...props]
                              })
                            }}
                            placeholder="Valor"
                          />
                        </Card.Content>
                      </Card>
                    ))}
                </Card.Group>


                <Divider />

                <Divider />
                <Button
                  onClick={() => {
                    values.prestadoresServico = [...servicos]
                    submit()
                  }}
                  floated="right"
                  disabled={!isValid()}
                  primary
                >
                  <Icon name="save" />
                  Salvar
              </Button>
                <Button onClick={() => handleCancel()} floated="right" negative>
                  <Icon name="cancel" />
                  Cancelar
              </Button>
              </Form>
            )}
        </FormService>
      </Grid.Column>
    </Grid>
  );
};

const withStateServico = withState('servicos', 'setServico', [])


export default compose(
  withStateSearch,
  withStateServico,
  withHandlerSearch)(PrestadorForm);
