import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination } from 'semantic-ui-react';

import { paginationReset, paginationUpdate } from '../../actions/pagination';

class PaginationContainer extends Component {
  constructor(props) {
    super(props);
    this.handlePageChange = this.handlePageChange.bind(this);
  }

  handlePageChange(ev, data) {
    const page = data.activePage === 0 ? data.activePage : data.activePage - 1;
    this.props.update(page, 0);

    this.props.handlePageChange();
  }

  render() {
    const { currentPage, totalPages } = this.props;

    return (
      <Pagination
        onPageChange={this.handlePageChange}
        defaultActivePage={currentPage + 1}
        totalPages={totalPages}
      />
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    reset: () => dispatch(paginationReset()),
    update: (page, total) => dispatch(paginationUpdate(page, total)),
  };
};

const mapStateToProps = state => {
  return {
    currentPage: state.pagination.currentPage,
    totalPages: state.pagination.totalPages,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(
  PaginationContainer,
);
