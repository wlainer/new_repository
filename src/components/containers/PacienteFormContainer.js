import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { get, clearItem, save } from '../../actions/api';

import AppFormWrapper from '../AppFormWrapper';
import PacienteForm from '../PacienteForm';

const PacienteContainer = props => <PacienteForm {...props} />;

const mapDispatchToProps = endpoint => dispatch => {
  return {
    get: id => dispatch(get(endpoint)(id)),
    save: data => dispatch(save(endpoint)(data)),
    clearItem: () => dispatch(clearItem()),
  };
};

const mapStateToProps = state => {
  return {
    item: state.api.item,
    isLoading: state.api.isLoading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps('pacientes'))(
  AppFormWrapper(PacienteContainer),
);
