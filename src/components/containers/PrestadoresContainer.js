import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import AppListWrapper from '../AppListWrapper';

import { getAll, exclude } from '../../actions/api';
import { paginationReset } from '../../actions/pagination';
import { searchBarReset } from '../../actions/searchBar';

const mapDispatchToProps = endpoint => dispatch => {
  return {
    getAll: () => dispatch(getAll(endpoint)()),
    paginationReset: () => dispatch(paginationReset()),
    searchBarReset: () => dispatch(searchBarReset()),
    exclude: (id) => dispatch(exclude(endpoint)(id)),
  };
};

const mapStateToProps = state => {
  return {
    itemList: state.api.items,
    isLoading: state.api.isLoading,
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps('prestadores'))(AppListWrapper),
);
