import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import SearchBar from '../SearchBar';
import { searchBarReset, searchBarUpdate } from '../../actions/searchBar';

class SearchBarContainer extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(id, idx) {
    return data => {
      const item = Object.assign({}, this.props.searchParams[idx]);
      item[id] = data.value;
      this.props.update(idx, item);
    };
  }

  render() {
    const { searchParams, reset, fields, onSearch } = this.props;

    return (
      <SearchBar
        fields={fields}
        items={searchParams}
        handleChange={this.handleChange}
        onReset={reset}
        onSearch={onSearch}
      />
    );
  }
}

SearchBarContainer.propTypes = {
  fields: PropTypes.arrayOf(PropTypes.object),
};

const mapDispatchToProps = dispatch => {
  return {
    reset: () => dispatch(searchBarReset()),
    update: (idx, value) => dispatch(searchBarUpdate(idx, value)),
  };
};

const mapStateToProps = state => {
  return {
    searchParams: state.searchBar.searchParams,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBarContainer);
