import React from 'react';
import { Dimmer, Image, Loader, Segment } from 'semantic-ui-react';
require('../assets/short-paragraph.png')

const Loading = () => (
  <Dimmer.Dimmable as={Segment} dimmed={true}>
    <Dimmer active={true} inverted>
      <Loader>Loading</Loader>
    </Dimmer>

    <p>
      <Image src="/short-paragraph.png" />
    </p>
    <p>
      <Image src="/short-paragraph.png" />
    </p>
    <p>
      <Image src="/short-paragraph.png" />
    </p>
    <p>
      <Image src="/short-paragraph.png" />
    </p>
  </Dimmer.Dimmable>
);

export default Loading;
