import React from 'react';
import { Table, Button, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';

import { compose, withHandlers, withState } from 'recompose'
import { get } from 'lodash'

import { searchBarUpdateSort } from '../actions/searchBar'
import Pagination from './containers/PaginationContainer'

import moment from 'moment';

const checkIfIsDate = value => {
  if (moment(value, moment.ISO_8601, true).isValid())
    return moment(value).format('L')
    else
    return value
}


const TableHeader = ({headers, sort, updateSort, onSearch }) => (
  <Table.Header>
  <Table.Row>
    {headers.map((header, idx) => (
      <Table.HeaderCell sorted={sort.column === header ? sort.direction : null} onClick={() => updateSort(header, onSearch)} key={idx}>{header}</Table.HeaderCell>
    ))}
    <Table.HeaderCell key={headers.length + 1}></Table.HeaderCell>
  </Table.Row>
</Table.Header>
)

const TableBody = ({items, fields, onEdit, onDelete  }) => (
  <Table.Body>
  {items.map((item, idx) => {
    const rows = [];
    fields.forEach((key, i) => {
      if (typeof item[key] !== 'object')
        rows.push(
          <Table.Cell collapsing key={`${idx}_${i}`}>
            {checkIfIsDate(get(item, key, ""))}
          </Table.Cell>,
        )
    },
    );
    rows.push(
      <Table.Cell key={fields.length + 1}>
        <Button
          onClick={() => onEdit(items[idx])}
          color="blue"
          size="mini"
        >
          <Icon name="edit" />Editar
        </Button>
        <Button
          onClick={() => onDelete(items[idx])}
          color="red"
          size="mini"
        >
          <Icon name="trash" />Excluir
        </Button>
      </Table.Cell>,
    );
    return <Table.Row key={idx}>{rows}</Table.Row>;
  })}
</Table.Body>
)

const TableFooter = ({headers, onSearch}) => (
  <Table.Footer>
  <Table.Row>
    <Table.HeaderCell colSpan={headers.length + 1} textAlign="center">
      <Pagination handlePageChange={onSearch} />
    </Table.HeaderCell>
  </Table.Row>
</Table.Footer>
)



const MyTable = (props) => {
  return (
    <Table color="blue" sortable striped>
      <TableHeader {...props} />
      <TableBody {...props} />
      <TableFooter {...props} />
    </Table>
  );
};


const withSortState = withState('sort', 'setSort', {
  column: '',
  direction: 'ascending'
})
const withHandle = withHandlers({
  updateSort: (({ setSort, headers, fields, dispatch }) => (column, onSearch) => {
    setSort(state => {
      const index = headers.findIndex(field => field === column)
      const direction = state.direction === 'ascending' ? 'descending' : 'ascending'
      dispatch(searchBarUpdateSort({ column: fields[index], direction: direction === 'ascending' ? 'asc' : 'desc' }))
      setTimeout(onSearch, 0)
      return {
        direction,
        column
      }
    })
  })
})



export default compose(
  connect(null, dispatch => ({ dispatch })),
  withSortState,
  withHandle,
)(MyTable)
