import React from 'react';
import {
  Button,
  Form,
  Grid,
  Divider,
  Icon,
  Input,
} from 'semantic-ui-react';

import FormService from '../utils/form';

const PacienteForm = ({ item, handleSubmit, handleCancel, isLoading }) => (
  <Grid celled>
    <Grid.Column>
      <FormService
        initialValues={item}
        onSubmit={handleSubmit}
        validations={{
          matricula: [value => !value && 'Obrigatório'],
          paciente: [value => !value && 'Obrigatório'],
          titular: [value => !value && 'Obrigatório'],
        }}
      >
        {({ getValue, getError, setValue, submit, isValid, isTouched }) => (
          <Form loading={isLoading}>
            <Form.Field
              error={isTouched('matricula') && getError('matricula')}
              required
            >
              <label>Matrícula</label>
              <Input
                placeholder="Matrícula"
                onChange={e => setValue('matricula', e.target.value)}
                value={getValue('matricula')}
              />
            </Form.Field>
            <Form.Field
              error={isTouched('paciente') && getError('paciente')}
              required
            >
              <label>Paciente</label>
              <Input
                placeholder="Paciente"
                onChange={e => setValue('paciente', e.target.value)}
                value={getValue('paciente')}
              />
            </Form.Field>
            <Form.Field
              error={isTouched('titular') && getError('titular')}
              required
            >
              <label>Titular</label>
              <Input
                placeholder="Titular"
                onChange={e => setValue('titular', e.target.value)}
                value={getValue('titular')}
              />
            </Form.Field>
            <Divider />
            <Button
              onClick={() => submit()}
              floated="right"
              disabled={!isValid()}
              primary
            >
              <Icon name="save" />
              Salvar
            </Button>
            <Button onClick={() => handleCancel()} floated="right" negative>
              <Icon name="cancel" />
              Cancelar
            </Button>
          </Form>
        )}
      </FormService>
    </Grid.Column>
  </Grid>
);

export default PacienteForm;
