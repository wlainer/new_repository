import React from 'react';
import PropTypes from 'prop-types';

import { get } from 'lodash';

import {
  Button,
  Grid,
  Dimmer,
  Loader,
  Header,
  Icon,
  Modal,
  Breadcrumb,
} from 'semantic-ui-react';

import MyTable from './Table';
import SearchBar from './containers/SearchBarContainer';

class AppListWrapper extends React.Component {
  constructor(props) {
    super(props);

    this.handleActions = this.handleActions.bind(this);
    this.toggleModal = this.toggleModal.bind(this);

    this.state = {
      openModal: false,
    };
  }

  componentWillMount() {
    const { location } = this.props;

    const reset = get(location, 'state.reset', true);

    if (reset) {
      this.props.paginationReset();
      this.props.searchBarReset();
    }
  }

  componentDidMount() {
    this.props.getAll();
  }

  toggleModal() {
    const modal = this.state.openModal;
    this.setState({ openModal: !modal });
  }

  handleActions(action) {
    return value => {
      const { location, history } = this.props;
      switch (action) {
        case 'new':
          history.push(`${location.pathname}/new`);
          break;
        case 'edit':
          history.push(`${location.pathname}/${value.id}/edit`);
          break;
        case 'onDelete':
          this.setState({ object: value });
          this.toggleModal();
          break;
        case 'delete':
          const { object } = this.state;
          this.props.exclude(object.id);
          this.toggleModal();
          this.setState({ object: {} });
          break;
        default:
      }
    };
  }

  render() {
    const { openModal } = this.state;

    const {
      itemList,
      fields,
      isLoading,
      getAll,
      headers,
      searchFields,
      location,
    } = this.props;

    return (
      <div>
        <Dimmer.Dimmable dimmed={isLoading}>
          <Dimmer active={isLoading} inverted>
            <Loader>Carregando...</Loader>
          </Dimmer>

          <Grid columns="equal" verticalAlign="middle">
            <Grid.Column floated="left">
              <Breadcrumb>
                <Breadcrumb.Section>Home</Breadcrumb.Section>
                <Breadcrumb.Divider />
                <Breadcrumb.Section>Pacientes</Breadcrumb.Section>
              </Breadcrumb>
            </Grid.Column>
            <Grid.Column floated="right">
              <Button
                size="small"
                floated="right"
                onClick={this.handleActions('new')}
              >
                Novo
              </Button>
            </Grid.Column>
          </Grid>

          <SearchBar fields={searchFields} onSearch={getAll} />

          <MyTable
            onSearch={getAll}
            headers={headers}
            fields={fields}
            items={itemList}
            onEdit={this.handleActions('edit')}
            onDelete={this.handleActions('onDelete')}
          />
        </Dimmer.Dimmable>

        <Modal open={openModal} basic size="small">
          <Header icon="trash" content="Confirmação!" />
          <Modal.Content>
            <p>Deseja excluir o registro selecionado?</p>
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={this.toggleModal} basic color="red" inverted>
              <Icon name="remove" /> Não
            </Button>
            <Button
              onClick={this.handleActions('delete')}
              color="green"
              inverted
            >
              <Icon name="checkmark" /> Sim
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}

export default AppListWrapper;
