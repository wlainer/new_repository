import * as types from '../constants/actionTypes';

import Api from '../utils/api';
import { getUrlParams } from '../utils/pagination';

import { paginationUpdate } from './pagination';

export function getAll(endpoint) {
  return () => {
    return async (dispatch, getState) => {
      dispatch(fetchingApi());

      const size = getState().pagination.size;
      const currentPage = getState().pagination.currentPage;
      const searches = getUrlParams(getState().searchBar);


      const items = await Api.getAll(`${endpoint}?page=${currentPage}&size=${size}&${searches}`);
      dispatch(fetchApiSuccess(items.content, null));
      dispatch(paginationUpdate(items.number, items.totalPages));
    };
  }
}

export function get(endpoint) {
  return id => {
    return async dispatch => {
      dispatch(fetchingApi());

      const item = await Api.get(endpoint, id);
      dispatch(fetchApiSuccess(null, item));
    };
  }
}

export function save(endpoint) {
  return data => {
    return async dispatch => {
      dispatch(fetchingApi());

      if (!data.headers)
        await Api.save(endpoint, data);
      else {
        Api.headers(data.headers);
        await Api.save(endpoint, data.form);
      }

      dispatch(fetchApiSuccess());
      dispatch(getAll(endpoint)());
    };
  }
}

export function exclude(endpoint) {
  return id => {
    return async dispatch => {
      dispatch(fetchingApi());

      const item = await Api.delete(endpoint, id);
      dispatch(fetchApiSuccess(null, item));
      dispatch(getAll(endpoint)());
    };
  }
}

export async function search(url) {
  return await Api.search(url);
}

export function clear() {
  return {
    type: types.API_CLEAR,
  };
}

export function clearItem() {
  return {
    type: types.API_CLEAR_ITEM,
  };
}

function fetchingApi() {
  return {
    type: types.API_FETCH,
    isLoading: true,
  };
}

function fetchApiSuccess(data, item) {
  const temp = {};
  if (!!data) temp.items = data;

  if (!!item) temp.item = item;

  return {
    type: types.API_FETCH_SUCCESS,
    isLoading: false,
    ...temp,
  };
}
