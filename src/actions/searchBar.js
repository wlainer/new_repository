import * as types from '../constants/actionTypes';

export function searchBarUpdate(idx, searchValue) {
  return {
    type: types.SEARCHBAR_UPDATE,
    item: searchValue,
    index: idx,
  };
}

export function searchBarUpdateSort(sort) {
  return {
    type: types.SEARCHBAR_UPDATE_SORT,
    sort
  };
}

export function searchBarReset() {
  return {
    type: types.SEARCHBAR_RESET,
  };
}
