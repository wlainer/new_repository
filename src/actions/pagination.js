import * as types from '../constants/actionTypes';

export function paginationUpdate(page, totalPages) {
  const pagInfo = {}
  if (page != null) pagInfo.currentPage = page

  if (totalPages != null) pagInfo.totalPages = totalPages


  return {
    type: types.PAGINATION_UPDATE,
    ...pagInfo
  };
}

export function paginationReset() {
  return {
    type: types.PAGINATION_RESET,
  };
}
